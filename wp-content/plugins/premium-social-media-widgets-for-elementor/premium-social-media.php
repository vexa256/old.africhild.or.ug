<?php
/*
Plugin Name: Premium Social Media Widgets for Elementor
Description: Premium Social Media Widgets for Elementor Plugin Includes Behance, Facebook, Instagram and Twitter widgets for Elementor Page Builder with tons of customization options.
Plugin URI: https://plugins.leap13.com/premium-social-media-widgets-for-elementor/
Version: 1.0.1
Author: Leap13
Author URI: http://leap13.com/
Text Domain: premium-social-media
Domain Path: /languages
License: GNU General Public License v3.0
*/

if (!function_exists('add_action')) {
    die('WordPress not Installed'); // if WordPress not installed kill the page.   
}

if ( ! defined('ABSPATH') ) exit; // Deny Direct Access

// Define Plugin Constants
define('PREMIUM_SOCIAL_VERSION', '1.0.1');
define('PREMIUM_SOCIAL_URL', plugins_url('/', __FILE__));
define('PREMIUM_SOCIAL_PATH', plugin_dir_path(__FILE__));
define('PREMIUM_SOCIAL_FILE', __FILE__);
define('PREMIUM_SOCIAL_BASENAME', plugin_basename(__FILE__));


// Define class 'Premium_Social_Media' if not Exists
if( ! class_exists( 'Premium_Social_Media' ) ){
    
    /*
     * Intialize and Sets up the plugin
     */
    class Premium_Social_Media {
       
        private static $instance = null;

        protected $ps_elements_keys = ['premium-social-twitter','premium-social-instagram', 'premium-social-facebook', 'premium-social-behance'];
        
        /**
		 * Sets up needed actions/filters for the plug-in to initialize.
		 * @since 1.0.0
		 * @access public
		 * @return void
		 */
        public function __construct()
        {
            // Check if Elementor installed
            add_action( 'init', array( $this, 'elementor_check' ), 0 );
            
            // Load the plugin setup core
            add_action( 'plugins_loaded', array( $this, 'premium_social_media_setup' ) );
            
            // Load the widgets file
            add_action( 'elementor/widgets/widgets_registered', array( $this, 'premium_social_media_widgets_register') );
            
            // Create new Elementor category
            add_action( 'elementor/widgets/widgets_registered', array( $this, 'initiate_premium_social_media_category') );
        
            // Enqueue the required stylesheets file
            add_action( 'elementor/frontend/after_enqueue_styles', array( $this, 'premium_social_media_enqueue_styles' ) );
            
            // Register the required JS files
            add_action( 'elementor/frontend/after_register_scripts', array( $this, 'premium_social_media_register_scripts') );
        }
        
        /**
         * Shows an admin notice if Elementor is not installed
         * @since 1.0.0
         * @access public
         * @return void
         */
        public function elementor_check() {
            
            if ( ! defined( 'ELEMENTOR__FILE__' ) ) {
                
                require ( PREMIUM_SOCIAL_PATH . 'includes/class-tgm-plugin-activation.php' );
                
                add_action( 'tgmpa_register', array( $this, 'register_required_plugins' ) );
                
            }
            
        }
        
        /**
		 * Register required plugins for Premium Social Media for Elementor
		 * @since 1.0.0
         * @access public
		 * @return void
		 */
		public function register_required_plugins() {

			$plugins = array(
				array(
					'name'     => 'Elementor',
					'slug'     => 'elementor',
					'required' => true,
				),
			);

			$config = array(
				'id'           => 'premium-social-media',
				'default_path' => '',
				'menu'         => 'tgmpa-install-plugins',
				'parent_slug'  => 'plugins.php',
				'capability'   => 'manage_options',
				'has_notices'  => true,
				'dismissable'  => true,
				'dismiss_msg'  => '',
				'is_automatic' => false,
				'strings'      => array(
					'notice_can_install_required'     => _n_noop(
						'Premium Social Media for Elementor requires the following plugin: %1$s.',
						'Premium Social Media for Elementor requires the following plugins: %1$s.',
						'premium-social-media'
					),
					'notice_can_install_recommended'  => _n_noop(
						'Premium Social Media for Elementor recommends the following plugin: %1$s.',
						'Premium Social Media for Elementor recommends the following plugins: %1$s.',
						'premium-social-media'
					),
				),
			);

			tgmpa( $plugins, $config );

		}
        
        /**
         * Installs translation text domain and checks if Elementor is installed
         * @since 1.0.0
         * @access public
         * @return void
         */
        public function premium_social_media_setup() {
    
            // Loads text domain
            load_plugin_textdomain( 'premium-social-media', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

            // Requires admins settings
            require_once ( PREMIUM_SOCIAL_PATH . 'admin/class-settings.php');
            
            // Requires addons responsive styling based on Elementor responsive breakpoints
            require_once ( PREMIUM_SOCIAL_PATH . 'includes/class-responsive.php' );
    
        }
        
        /** Enqueue required CSS files
         * @since 1.0.0
         * @access public
         */
        public function premium_social_media_enqueue_styles() {
            
            wp_enqueue_style(
                'premium-social-media',
                PREMIUM_SOCIAL_URL . 'assets/css/premium-social.css',
                array(),
                PREMIUM_SOCIAL_VERSION,
                'all'
            );
            
            wp_enqueue_style(
                'ps-prettyphoto',
                PREMIUM_SOCIAL_URL . 'assets/css/prettyphoto.css',
                array(),
                PREMIUM_SOCIAL_VERSION,
                'all'
            );
            
        }
        
        /** Returns an array with true values
         * @since 1.0.0
         * @access public
         * @return array
         */
        public function fill_default_keys() {
            
            $default_array = array_fill_keys( $this->ps_elements_keys, true );
            
            $check_active_modules = get_option( 'ps_settings', $default_array );
            
            return $check_active_modules;
            
        }
        
        /** Registers the required JS files
         * @since 1.0.0
         * @access public
         */
        public function premium_social_media_register_scripts() {

            $check_component_active = $this->fill_default_keys();

            if( $check_component_active['premium-social-twitter'] || $check_component_active['premium-social-facebook'] ) {
            
                wp_register_script( 'ps-codebird-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/codebird.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'ps-dot-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/doT.min.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'ps-socialfeed-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/jquery.socialfeed.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'ps-masonry-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/masonry.min.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'ps-imagesloaded-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/imagesloaded.min.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'premium-social-media-js', PREMIUM_SOCIAL_URL . 'assets/js/premium-social.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

            }

            if( $check_component_active['premium-social-behance'] ) {

                wp_register_script( 'ps-behance-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/embed.behance.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'premium-social-media-js', PREMIUM_SOCIAL_URL . 'assets/js/premium-social.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

            }

            if( $check_component_active['premium-social-instagram'] ) {

                wp_register_script( 'ps-masonry-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/masonry.min.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'ps-imagesloaded-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/imagesloaded.min.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'ps-prettyphoto-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/prettyPhoto.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'ps-instafeed-js', PREMIUM_SOCIAL_URL . 'assets/js/lib/instafeed.min.js', array(), PREMIUM_SOCIAL_VERSION, true );

                wp_register_script( 'premium-social-media-js', PREMIUM_SOCIAL_URL . 'assets/js/premium-social.js', array('jquery'), PREMIUM_SOCIAL_VERSION, true );

            }

        }
        
        /** Requires widgets files
         * @since 1.0.0
         * @access public
         */
        public function premium_social_media_widgets_register() {

            $check_component_active = $this->fill_default_keys();
            
            // Require Behance widget File
            if( $check_component_active['premium-social-behance'] ) {
                require_once ( PREMIUM_SOCIAL_PATH . 'addons/premium-behance.php' );    
            }

            // Require Facebook widget File
            if( $check_component_active['premium-social-facebook'] ) {
                require_once ( PREMIUM_SOCIAL_PATH . 'addons/premium-facebook.php' );
            }
            
            // Require Instagram widget File
            if( $check_component_active['premium-social-instagram'] ) {
                require_once ( PREMIUM_SOCIAL_PATH . 'addons/premium-instagram.php' );
            }

            // Require Twitter widget File
            if( $check_component_active['premium-social-twitter'] ) {
                require_once ( PREMIUM_SOCIAL_PATH . 'addons/premium-twitter.php' );
            }
            
        }
        
        /** Creates a new category in Elementor editor area
         * @since 1.0.0
         * @access public
         */
        public function initiate_premium_social_media_category() {
            Elementor\Plugin::instance()->elements_manager->add_category(
                'premium-social-media',
                array(
                    'title' => esc_html__('Premium Social Media', 'premium-social-media')
                ),
            1);
        }
        
        /** Creates an object of the plugin main class if not created yet
         * @since 1.0.0
         * @access public
         * @return object
         */
        public static function get_instance() {
            
            if ( self::$instance == null ) {
                
                self::$instance = new self;
                
            }
            
            return self::$instance;
            
        }
        
    }
}

/*
 * Return an object of Premium_Social_Media
 * 
 * @return object
 */
if ( ! function_exists('premium_social_media') ) {
    
    function premium_social_media() {
        
        return Premium_Social_Media::get_instance();
        
    }
}
premium_social_media();