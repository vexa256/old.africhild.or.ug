# Premium Social Media Widgets for Elementor

Premium Social Media Widgets for Elementor Plugin Includes Behance, Facebook, Instagram and Twitter widgets for Elementor Page Builder.
0

# ChangeLog

= 1.0.1 =

- Tweak: Added responsive controls for `Images`, `Posts` and `Tweets` per Row option in Instagram, Facebook and Twitter widgets.
- Fixed: Different images height when `Masonry` option is disabled in Intagram widget.
- Fixed: Twitter Feed widget does not work due to API issue.

- New: Added `Vertical Scroll` widget.

* Init