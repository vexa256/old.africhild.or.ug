<?php

namespace PremiumSocialWidgets;

if( ! defined( 'ABSPATH' ) ) exit(); // Exit if accessed directly

/*
* Intialize and Sets up plugin admin settings
*/
class Premium_Social_Settings {
    
    private static $instance;
    
    private $ps_defaults;
    
    private $ps_settings;
    
    private $ps_get_settings;
    
    protected $page_slug    = 'premium-social';

    public $ps_keys = [ 'premium-social-twitter', 'premium-social-instagram', 'premium-social-facebook', 'premium-social-behance' ];
    
    
    public function __construct() {

        // Creates dashboard menu
        add_action( 'admin_menu', array( $this,'premium_social_admin_menu') );

        // Enqueues required admin scripts and styles
        add_action('admin_enqueue_scripts', array( $this, 'premium_social_admin_scripts' ) );

        // Saves admin settings using AJAX
        add_action( 'wp_ajax_premium_social_save_settings', array( $this, 'ajax_premium_social_save_settings' ) );
        
    }
    
    /*
     * Enqueues required assets in plugin admin page only
     * @since 1.0.0
     * @access public
     * @return void
     */
    public function premium_social_admin_scripts() {
    
        $current_screen = get_current_screen();
        
        if( strpos( $current_screen->id, $this->page_slug ) !== false ){

            wp_enqueue_style( 
                'ps-admin',
                PREMIUM_SOCIAL_URL . 'admin/css/admin.css'
            );

            wp_enqueue_style(
                'ps-sweetalert-style',
                PREMIUM_SOCIAL_URL . 'admin/js/sweetalert2/css/sweetalert2.min.css'
            );
            
            wp_enqueue_script(
                'ps-sweetalert-core',
                PREMIUM_SOCIAL_URL . 'admin/js/sweetalert2/js/core.js',
                array( 'jquery' ),
                PREMIUM_SOCIAL_VERSION,
                true
            );
            
            wp_enqueue_script(
                'ps-sweetalert',
                PREMIUM_SOCIAL_URL . 'admin/js/sweetalert2/js/sweetalert2.min.js',
                array( 'jquery', 'ps-sweetalert-core' ),
                PREMIUM_SOCIAL_VERSION,
                true
            );
            
            wp_enqueue_script( 
                'ps-admin-js',
                PREMIUM_SOCIAL_URL . 'admin/js/admin.js',
                array('jquery'),
                PREMIUM_SOCIAL_VERSION,
                true
            );

        }
    }

    /*
     * Creates admin menu
     * @since 1.0.0
     * @access public
     * @return void
     */
    public function premium_social_admin_menu() { 

       add_menu_page( 
          'Premium Social Widgets',
          'Premium Social Widgets',
          'manage_options',
          'premium-social',
          array( $this , 'premium_social_elements_page' ),
          '',
          100
        );

    }

    /*
     * Plugin admin page content
     * @since 1.0.0
     * @access public
     * @return void
     */
    public function premium_social_elements_page(){

        $js_info = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' )
		);

        //Sends widget settings to admin.js file
		wp_localize_script( 'ps-admin-js', 'settings', $js_info );
       
	   $this->ps_defaults = array_fill_keys( $this->ps_keys, true );
       
	   $this->ps_get_settings = get_option( 'ps_settings', $this->ps_defaults );
       
	   $ps_new_settings = array_diff_key( $this->ps_defaults, $this->ps_get_settings );
       
	   if( ! empty( $ps_new_settings ) ) {
           
           $ps_updated_settings = array_merge( $this->ps_get_settings, $ps_new_settings );

            update_option( 'ps_settings', $ps_updated_settings );

	   }

	   $this->ps_get_settings = get_option( 'ps_settings', $this->ps_defaults );
       
	?>

	<div class="wrap">
        <div class="response-wrap"></div>
            <form action="" method="POST" id="ps-settings" name="ps-settings">
                <div class="ps-header-wrapper">
                    <div class="ps-title-left">
                        <h1 class="ps-title-main"><?php echo __('Premium Social Widgets for Elementor','premium-social-media'); ?></h1>
                        <h3 class="ps-title-sub"><?php echo __('Thank you for using Premium Social Widgets. This plugin has been developed by Leap13 and we hope you enjoy using it.','premium-social-media'); ?></h3>
                    </div>
                </div>
                <div class="ps-settings-tabs">
                    <div id="ps-modules" class="ps-settings-tab">
                        <div>
                            <br>
                            <input type="checkbox" class="ps-checkbox" checked="checked">
                            <label>Enable/Disable All</label>
                        </div>
                        <table class="ps-elements-table">
                            <tbody>
                                <tr>
                                    <th><?php echo esc_html__('Premium Behance Feed', 'premium-social-media'); ?></th>
                                    <td>
                                        <label class="switch">
                                                <input type="checkbox" id="premium-social-behance" name="premium-social-behance" <?php checked(1, $this->ps_get_settings['premium-social-behance'], true) ?>>
                                                <span class="slider round"></span>
                                        </label>
                                    </td>
                                
                                    <th><?php echo esc_html__('Premium Facebook Feed', 'premium-social-media'); ?></th>
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" id="premium-social-facebook" name="premium-social-facebook" <?php checked(1, $this->ps_get_settings['premium-social-facebook'], true) ?>>
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                </tr>
                            
                                <tr>
                                    <th><?php echo esc_html__('Premium Instagram Feed', 'premium-social-media'); ?></th>
                                    <td>
                                        <label class="switch">
                                                <input type="checkbox" id="premium-social-instagram" name="premium-social-instagram" <?php checked(1, $this->ps_get_settings['premium-social-instagram'], true) ?>>
                                                <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <th><?php echo esc_html__('Premium Twitter Feed', 'premium-social-media'); ?></th>
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" id="premium-social-twitter" name="premium-social-twitter" <?php checked(1, $this->ps_get_settings['premium-social-twitter'], true) ?>>
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="submit" value="Save Settings" class="button ps-btn ps-save-button">
                    </div>
                </div>
            </form>
        </div>
	<?php
}

    /**
     * Updates elements settings option
     * @since 1.0.0
     * @access public
     * @return boolean
     */
    public function ajax_premium_social_save_settings() {

        if( isset( $_POST['fields'] ) ) {

            parse_str( $_POST['fields'], $settings );

        } else {

            return;

        }

        $this->ps_settings = array(
            'premium-social-twitter'      => intval( $settings['premium-social-twitter'] ? 1 : 0),
            'premium-social-instagram'    => intval( $settings['premium-social-instagram'] ? 1 : 0),
            'premium-social-facebook'     => intval( $settings['premium-social-facebook'] ? 1 : 0),
            'premium-social-behance'      => intval( $settings['premium-social-behance'] ? 1 : 0),
            
        );

        update_option( 'ps_settings', $this->ps_settings );

        return true;

        die();
    }
    
    /** Creates an object of admin settings class if not created yet
    * @since 1.0.0
    * @access public
    * @return object
    */
    public static function get_instance() {
            
        if ( self::$instance == null ) {
                
            self::$instance = new self;
                
        }
            
        return self::$instance;
        
    }
}

// Return an object of admin settings class
if ( ! function_exists('premium_social_media_admin') ) {
    
    function premium_social_media_admin() {
        
        return Premium_Social_Settings::get_instance();
        
    }
}

//Triggers get instance function
premium_social_media_admin();