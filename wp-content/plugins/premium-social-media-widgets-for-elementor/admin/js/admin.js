//Go Between the Tabs
( function ( $ ){
    "use strict";
    
    $(".ps-checkbox").on("click", function(){
       if($(this).prop("checked") == true) {
           $(".ps-elements-table input").prop("checked", 1);
       }else if($(this).prop("checked") == false){
           $(".ps-elements-table input").prop("checked", 0);
       }
    });
   
    $( 'form#ps-settings' ).on( 'submit', function(e) {
		e.preventDefault();
		$.ajax( {
			url: settings.ajaxurl,
			type: 'post',
			data: {
				action: 'premium_social_save_settings',
				fields: $( 'form#ps-settings' ).serialize(),
			},
            success: function( response ) {
				swal(
				  'Settings Saved!',
				  'Click OK to continue',
				  'success'
				);
			},
			error: function() {
				swal(
				  'Oops...',
				  'Something Wrong!',
				);
			}
		} );

	} );
    
} )(jQuery);