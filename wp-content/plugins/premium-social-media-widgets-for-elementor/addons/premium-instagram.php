<?php

/**
 * Class: Premium_Instagram_Feed
 * Name: Instagram Feed
 * Slug: premium-social-instagram
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Premium_Instagram_Feed extends Widget_Base
{
    public function get_name() {
        return 'premium-social-instagram';
    }

    public function get_title() {
		return esc_html__('Premium Instagram Feed', 'premium-social-media');
	}
    
    public function is_reload_preview_required(){
        return true;
    }
    
    public function get_script_depends(){
        return [
            'ps-imagesloaded-js',
            'ps-prettyphoto-js',
            'ps-masonry-js',
            'ps-instafeed-js',
            'premium-social-media-js',
        ];
    }

    public function get_icon() {
        return 'fa fa-instagram';
    }

    public function get_categories() {
        return ['premium-social-media'];
    }

    // Adding the controls fields for the Instagram Feed
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
       
        $this->start_controls_section(
            'general_settings_section',
            [
                'label'         => esc_html__('Access Credentials', 'premium-social-media')
            ]
        );
        
        $this->add_control(
            'client_id',
            [
                'label'         => esc_html__('Client ID', 'premium-social-media'),
                'type'          => Controls_Manager::TEXT,
                'default'       => '8f051adcfa134a18bd6afecc7521ef44',
                'label_block'   => false,
                'description'   => '<a href="https://www.instagram.com/developer/" target="_blank">Get Client ID</a> by creating a new app or selecting an existing app ',
            ]
        );
                
        $this->add_control(
            'client_access_token',
            [
                'label'         => esc_html__('Access Token', 'premium-social-media'),
                'type'          => Controls_Manager::TEXT,
                'default'       => '2075884021.1677ed0.2fd28d5d3abf45d4a80534bee8376f4c',
                'label_block'   => false,
                'description'   => '<a href="http://instagram.pixelunion.net/" target="_blank">Get Access Token</a> by creating a new app or selecting an existing app ',
            ]
        );
        
        $this->add_control(
            'user_id',
            [
                'label'         => esc_html__('User Id', 'premium-social-media'),
                'type'          => Controls_Manager::TEXT,
                'default'       => '2075884021',
                'label_block'   => false,
                'description'   => 'Click <a href="https://codeofaninja.com/tools/find-instagram-user-id" target="_blank">Here</a> to get User ID',
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section(
            'query_section',
            [
                'label'         => esc_html__('Queries', 'premium-social-media')
            ]
        );
        
        $this->add_control(
            'search_for',
            [
                'label'         => esc_html__('Search for:', 'premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'user'          => esc_html__('User', 'premium-social-media'),
                    'location'      => esc_html__('Location', 'premium-social-media'),
                    'tagged'        => esc_html__('Tag', 'premium-social-media'),
                ],
                'default'       => 'user',
            ]
        );
        
        $this->add_control(
            'location_id',
            [
                'label'         => esc_html__('Location Id', 'premium-social-media'),
                'type'          => Controls_Manager::TEXT,
                'label_block'   => false,
                'default'       => '6889842',
                'description'   => 'Click <a href="http://docs.social-streams.com/article/118-find-instagram-location-id" target="_blank">Here</a> to find how to get location ID',
                'condition'     => [
                    'search_for' => 'location',
                ]
            ]
        );
        
        $this->add_control(
            'tag_name',
            [
                'label'         => esc_html__('Tag Name', 'premium-social-media'),
                'type'          => Controls_Manager::TEXT,
                'label_block'   => false,
                'default'       => 'sport',
                'condition'     => [
                    'search_for' => 'tagged',
                ]
            ]
        );
        
        $this->add_control(
            'sort_by',
            [
                'label'         => esc_html__('Sort By:', 'premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'none'              => esc_html__('none', 'premium-social-media'),
                    'most-recent'       => esc_html__('Most Recent', 'premium-social-media'),
                    'least-recent'      => esc_html__('Least Recent', 'premium-social-media'),
                    'most-liked'        => esc_html__('Most Liked', 'premium-social-media'),
                    'least-liked'       => esc_html__('Least Liked', 'premium-social-media'),
                    'most-commented'    => esc_html__('Most Commented', 'premium-social-media'),
                    'least-commented'   => esc_html__('Least Commented', 'premium-social-media'),
                    'random'            => esc_html__('Random', 'premium-social-media'),
                ],
                'default'       => 'none',
            ]
        );
        
        $this->add_control(
            'link',
			[
				'label'         => esc_html__( 'Enable Redirection', 'premium-social-media' ),
				'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Redirect to Photo Link on Instgram','premium-social-media'),
			]
		);

		$this->add_control(
            'new_tab',
			[
				'label'         => esc_html__( 'Open in a New Tab', 'premium-social-media' ),
				'type'          => Controls_Manager::SWITCHER,
                'condition'     => [
                    'link'   => 'yes'
                ]
			]
		);
        
        $this->add_control(
            'popup',
			[
				'label'         => esc_html__( 'Modal Image', 'premium-social-media' ),
				'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Modal image works only on the frontend', 'premium-social-media'),
                'condition'     => [
                    'link!'   => 'yes'
                ]
			]
		);
        
        $this->add_control(
            'show_likes',
			[
				'label'         => esc_html__( 'Likes Info', 'premium-social-media' ),
				'type'          => Controls_Manager::SWITCHER,
			]
		);
        
        $this->add_control(
            'show_comments',
			[
				'label'         => esc_html__( 'Comments Info', 'premium-social-media' ),
				'type'          => Controls_Manager::SWITCHER,
			]
		);
        
        $this->end_controls_section();
        
        $this->start_controls_section(
            'layout_settings_section',
            [
                'label'         => esc_html__('Layout', 'premium-social-media'),
            ]
        );
        
        $this->add_control(
            'img_number',
            [
                'label'         => esc_html__('Maximum Images Number', 'premium-social-media'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 6,
            ]
        );
        
        $this->add_control(
            'resolution',
            [
                'label'         => esc_html__('Image Resolution', 'premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'thumbnail'             => esc_html__('Thumbnail (150x150)', 'premium-social-media'),
                    'low_resolution'        => esc_html__('Low (306x306)', 'premium-social-media'),
                    'standard_resolution'   => esc_html__('Standard (612x612)', 'premium-social-media'),

                ],
                'default'       => 'standard_resolution',
            ]
        );
        
        $this->add_control(
            'masonry',
			[
				'label'         => esc_html__( 'Masonry', 'premium-social-media' ),
				'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes'
			]
		);
        
        $this->add_responsive_control('column_number',
            [
                'label'             => __('Images per Row', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::SELECT,
                'options'           => [
                    '100%'  => __('1 Column', 'premium-addons-for-elementor'),
                    '50%'   => __('2 Columns', 'premium-addons-for-elementor'),
                    '33.33%'=> __('3 Columns', 'premium-addons-for-elementor'),
                    '25%'   => __('4 Columns', 'premium-addons-for-elementor'),
                    '20%'   => __('5 Columns', 'premium-addons-for-elementor'),
                    '16.667%'=> __('6 Columns', 'premium-addons-for-elementor'),
                ],
                'desktop_default'   => '33.33%',
				'tablet_default'    => '50%',
				'mobile_default'    => '100%',
                'selectors'         => [
                    '{{WRAPPER}} .premium-insta-feed' => 'width: {{VALUE}}'
                ],
            ]
        );
        
        $this->add_control(
            'image_hover',
            [
                'label'         => esc_html__('Hover Image Effect', 'premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'none'   => esc_html__('None', 'premium-social-media'),
                    'zoomin' => esc_html__('Zoom In', 'premium-social-media'),
                    'zoomout'=> esc_html__('Zoom Out', 'premium-social-media'),
                    'scale'  => esc_html__('Scale', 'premium-social-media'),
                    'gray'   => esc_html__('Grayscale', 'premium-social-media'),
                    'blur'   => esc_html__('Blur', 'premium-social-media'),
                    'sepia'  => esc_html__('Sepia', 'premium-social-media'),
                    'bright' => esc_html__('Brightness', 'premium-social-media'),
                    'trans'  => esc_html__('Translate', 'premium-social-media'),
                ],
                'default'       => 'zoomin',
                'label_block'   => true
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section(
            'photo_box_style',
            [
                'label'         => esc_html__('Photo Box','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_responsive_control(
            'image_height',
            [
                'label'         => esc_html__('Image Height', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'range'             => [
                    'px'    => [
                        'min' => 50, 
                        'max' => 500,
                        ],
                    'em'    => [
                        'min' => 1, 
                        'max' => 100,
                    ]
                ],
                'default'       => [
                    'size'  => 300,
                    'unit'  => 'px'
                ],
                'condition'     => [
                    'masonry!'    => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-img-wrap img' => 'height: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->start_controls_tabs( 'tweet_box' );
        
        $this->start_controls_tab(
            'photo_box_normal',
            [
                'label'         => esc_html__('Normal', 'premium-social-media'),
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'photo_box_border',
                'selector'      => '{{WRAPPER}} .premium-insta-img-wrap',
            ]
        );
        
        $this->add_control(
            'photo_box_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-img-wrap' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'     => esc_html__('Shadow','premium-social-media'),
                    'name'      => 'photo_box_shadow',
                    'selector'  => '{{WRAPPER}} .premium-insta-img-wrap',
                ]
                );
        
        $this->add_responsive_control(
            'photo_box_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-img-wrap' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control(
            'photo_box_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-img-wrap' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();


        $this->start_controls_tab(
            'photo_box_hover',
            [
                'label'         => esc_html__('Hover', 'premium-social-media'),
            ]
        );
        
        $this->add_control(
            'overlay_background', 
            [
                'label'         => esc_html__('Overlay Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-info-wrap' => 'background-color: {{VALUE}};',
                ],
            ]
        );
                
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'photo_box_border_hover',
                'selector'      => '{{WRAPPER}} .premium-insta-feed-wrap:hover .premium-insta-img-wrap',
            ]
        );
        
        $this->add_control(
            'photo_box_border_radius_hover',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-feed-wrap:hover .premium-insta-img-wrap' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-social-media'),
                'name'          => 'photo_box_shadow_hover',
                'selector'      => '{{WRAPPER}} .premium-insta-feed-wrap:hover .premium-insta-img-wrap',
            ]
        );
        
        $this->add_responsive_control(
            'photo_box_margin_hover',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-feed-wrap:hover .premium-insta-img-wrap' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
 
        $this->end_controls_tab();

        $this->end_controls_tabs();
            
        $this->end_controls_section();

        $this->start_controls_section('photo_likes_style',
            [
                'label'         => esc_html__('Likes','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'show_likes' => 'yes'
                ]
            ]
        );
        
        $this->start_controls_tabs( 'likes' );
        
        $this->start_controls_tab(
            'likes_icon',
            [
                'label'         => esc_html__('Icon', 'premium-social-media'),
            ]
        );

        $this->add_control(
            'likes_color', 
            [
                'label'         => esc_html__('Icon Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-heart' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'likes_size',
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-heart' => 'font-size: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_control(
            'likes_background', 
            [
                'label'         => esc_html__('Background Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-heart' => 'background-color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'likes_border',
                'selector'      => '{{WRAPPER}} .premium-insta-heart',
            ]
        );
        
        $this->add_control(
            'likes_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-heart' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-social-media'),
                'name'          => 'likes_shadow',
                'selector'      => '{{WRAPPER}} .premium-insta-heart',
            ]
        );
        
        $this->add_responsive_control(
            'likes_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-heart' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
                
        $this->add_responsive_control(
            'likes_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-heart' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab(
            'likes_number',
            [
                'label'         => esc_html__('Number', 'premium-social-media'),
            ]
        );
        
        $this->add_control(
            'likes_number_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-likes' => 'color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'likes_number_type',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-insta-likes',
            ]
        );
        
        $this->add_control(
            'likes_number_background', 
            [
                'label'         => esc_html__('Background Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}}  .premium-insta-likes' => 'background-color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'likes_number_border',
                'selector'      => '{{WRAPPER}} .premium-insta-likes',
            ]
        );
        
        $this->add_control(
            'likes_number_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-likes' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-social-media'),
                'name'          => 'likes_number_shadow',
                'selector'      => '{{WRAPPER}} .premium-insta-likes',
            ]
        );
        
        $this->add_responsive_control(
            'likes_number_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-likes' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control(
            'likes_number_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-likes' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
        
        $this->start_controls_section(
            'photo_comments_style',
            [
                'label'         => esc_html__('Comments','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'show_comments' => 'yes'
                ]
            ]
        );
        
        $this->start_controls_tabs( 'comments' );
        
        $this->start_controls_tab(
            'comments_icon',
            [
                'label'         => esc_html__('Icon', 'premium-social-media'),
            ]
        );

        $this->add_control(
            'comment_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comment' => 'color: {{VALUE}};',
                    ],
                ]
            );

        $this->add_responsive_control(
            'comment_size',
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comment' => 'font-size: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_control(
            'comment_background', 
            [
                'label'         => esc_html__('Background Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comment' => 'background-color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'comments_border',
                'selector'      => '{{WRAPPER}} .premium-insta-comment',
            ]
        );
        
        $this->add_control(
            'comment_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comment' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-social-media'),
                'name'          => 'comments_shadow',
                'selector'      => '{{WRAPPER}} .premium-insta-comment',
            ]
        );
        
        $this->add_responsive_control(
            'comments_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comment' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
                
        $this->add_responsive_control(
            'comments_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comment' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab(
            'comments_number',
            [
                'label'         => esc_html__('Number', 'premium-social-media'),
            ]
        );
        
        $this->add_control(
            'comments_number_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comments' => 'color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'comments_number_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-insta-comments',
            ]
        );
        
        $this->add_control(
            'comments_number_background', 
            [
                'label'         => esc_html__('Background Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}}  .premium-insta-comments' => 'background-color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'comments_number_border',
                'selector'      => '{{WRAPPER}} .premium-insta-comments',
            ]
        );
        
        $this->add_control(
            'comments_number_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comments' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-social-media'),
                'name'          => 'comments_number_shadow',
                'selector'      => '{{WRAPPER}} .premium-insta-comments',
            ]
        );
        
        $this->add_responsive_control(
            'comments_number_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comments' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control(
            'comments_number_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-insta-comments' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
        
        $this->start_controls_section(
            'general_style',
            [
                'label'         => esc_html__('Container','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
            
        $this->add_control('container_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-instafeed-container' => 'background-color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'container_box_border',
                'selector'      => '{{WRAPPER}} .premium-instafeed-container',
            ]
        );
        
        $this->add_control(
            'container_box_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-instafeed-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'container_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-instafeed-container',
            ]
        );
        
        $this->add_responsive_control(
            'container_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-instafeed-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control(
            'container_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-instafeed-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_section();

                
    }
   
    /**
	 * renders the HTML content of the widget
	 * @return void
	 */
    protected function render() {
        $settings = $this->get_settings();
        
        $get_from = $settings['search_for'];
        
        $columns_class  = 'premium-insta-' . $settings['column_number'];
        
        $hover_effect   = 'premium-insta-' . $settings['image_hover'];
        
        $new_tab        = $settings['new_tab'] == 'yes' ? 'target="_blank"' : '' ;
        
        if( 'yes' == $settings['link'] ) {
            
            $link = '<a href="{{link}}"' . $new_tab . '></a>';
            
        } else {
            
            if( 'yes' == $settings['popup'] ) {
                
                $link = '<a href="{{image}}" data-rel="prettyPhoto[premium-insta-'.esc_attr($this->get_id()).']"></a>';
                
            } else {
                
                $link = '';
                
            }
            
        }
        
        if( 'yes' == $settings['show_likes'] ) {
            
            $likes = '<p> <i class="fa fa-heart premium-insta-heart" aria-hidden="true"></i> <span  class="premium-insta-likes">{{likes}}</span></p>';
            
        } else {
            
            $likes = '';
            
        }
        
        if( 'yes' == $settings['show_comments'] ) {
            
            $comments = '<p><i class="fa fa-comment premium-insta-comment" aria-hidden="true"></i><span class="premium-insta-comments">{{comments}}</span></p>';
            
        } else {
            
            $comments = '';
            
        }
        
        $client_id      = !empty( $settings['client_id'] ) ? esc_html ( $settings['client_id'] ) : '';
        
        $access_token   = !empty( $settings['client_access_token'] ) ? esc_html ( $settings['client_access_token'] ) : '';
        
        $location       = !empty( $settings['location_id'] ) ? esc_html( $settings['location_id'] ) : '';
        
        $user           = !empty( $settings['user_id'] ) ? esc_html ( $settings['user_id'] ) : '';
        
        $tag            = !empty( $settings['tag_name'] ) ? esc_html ( $settings['tag_name'] ) : '';
        
        $sort           = $settings['sort_by'];
        
        $res            = $settings['resolution'];
        
        $limit          = !empty( $settings['img_number'] ) ? esc_html ( $settings['img_number'] ) : 6;
        
        $instagram_settings = [
            'clientId'      => $client_id,
            'accesstok'     => $access_token,
            'get'           => $get_from,
            'location'      => $location,
            'user'          => $user,
            'tag'           => $tag,
            'sort'          => $sort,
            'limit'         => $limit,
            'res'           => $res,
            'hoverEff'      => $hover_effect,
            'likes'         => $likes,
            'comments'      => $comments,
            'link'          => $link,
            'id'            => 'premium-instafeed-container-' . $this->get_id(),
            'masonry'       => ( 'yes' == $settings['masonry'] ) ? true : false
        ];
        
        $id = $this->get_id();
        
        $this->add_render_attribute('premium_instagram', 'class', 'premium-instafeed-container' );
        
        $this->add_render_attribute('premium_instagram', 'data-settings', wp_json_encode($instagram_settings) );
        
        $this->add_render_attribute('premium_instagram_inner', 'id', 'premium-instafeed-container-' . $id );
        
        $this->add_render_attribute('premium_instagram_inner', 'class', 'premium-insta-grid' );
        
    ?>
    

    <?php if( empty ( $settings['client_id'] ) || empty ( $settings['client_access_token'] ) ) : ?>
        <div class="premium-social-error">
                <?php echo esc_html__('Please fill the required fields: Consumer Key & Consumer Secret','premium-social-media'); ?>
        </div>
        <?php else: ?>
        <div <?php echo $this->get_render_attribute_string('premium_instagram'); ?>>
            <div <?php echo $this->get_render_attribute_string('premium_instagram_inner'); ?>></div>
            <div class="premium-loading-feed">
                <div class="premium-loader"></div>
            </div>
        </div>
    
    <?php endif; ?>

    <?php
        
    }
}
Plugin::instance()->widgets_manager->register_widget_type( new Premium_Instagram_Feed() );