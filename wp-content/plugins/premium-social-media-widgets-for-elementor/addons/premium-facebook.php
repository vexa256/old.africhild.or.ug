<?php

/**
 * Class: Premium_Facebook_Feed
 * Name: Facebook Feed
 * Slug: premium-social-facebook
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Premium_Facebook_Feed extends Widget_Base {
	
    public function get_name() {
        return 'premium-social-facebook';
    }
    
    public function get_title() {
		return esc_html__('Premium Facebok Feed', 'premium-social-media');
	}

    public function get_icon() {
        return 'fa fa-facebook';
    }
    
    public function get_categories() {
        return ['premium-social-media'];
    }
    
    public function get_script_depends() {
        return [
            'ps-codebird-js',
            'ps-dot-js',
            'ps-socialfeed-js',
            'ps-masonry-js',
            'imagesloaded',
            'premium-social-media-js',
        ];
    }
    
    public function is_reload_preview_required() {
        return true;
    }
   
    // Adding the controls fields for the Facebook Feed
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
    
        $this->start_controls_section(
            'access_credentials_section',
            [
                'label'         => esc_html__('Access Credentials', 'premium-social-media')
            ]
        );

        $this->add_control(
            'access_token',
            [
                'label'         => esc_html__('Access Token', 'premium-social-media'),
                'default'       => 'EAACuIfHasYEBAJdezdsCLK9IgAHc6jX9DRa6r0M2itsr53m2g2d8Pk1NPwKtzFBwE15NlEHpCHXF1ZAYZA2KspJbJIrqgdMinNv4aSJbNL3RmZBFwMXe0AjnA7G1E0Vz4n06VFSroqdZAZCx6ck5LDRtgWy58cfzHrHDWNjRRVwZDZD',
                'description'   => 'Click <a href="https://www.youtube.com/watch?v=Zb8YWXlXo-k" target="_blank">Here</a> to know how to get your page access token',
                'type'          => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'account_settings_section',
            [
                'label'         => esc_html__('Account', 'premium-social-media')
            ]
        );
        
        $this->add_control(
            'account_id',
            [
                'label'         => __( 'Page Slug /User ID', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::TEXT,
                'default'       => '!leap13themes',
                'description'   => 'Account ID is prefixed by @, Page Slug is prefixed by !, click <a target="_blank" href="https://findmyfbid.com/"> here</a> to get account ID',
                'label_block'   => true,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'page_settings',
            [
                'label'         => esc_html__( 'Layout', 'premium-social-media' ),
            ]
        );

        $this->add_control(
            'layout_style',
            [
                'label'         => esc_html__('Style', 'premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'description'   => esc_html__( 'Choose the layout style for the posts', 'premium-social-media' ),
                'options'       => [
                    'list'          => esc_html__('List', 'premium-social-media'),
                    'masonry'       => esc_html__('Masonry', 'premium-social-media'),
                ],
                'default'       => 'list',
            ]
        );

        $this->add_responsive_control(
            'column_number',
            [
                'label'             => __('Posts/Row', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::SELECT,
                'options'           => [
                    '100%'  => __('1 Column', 'premium-social-media'),
                    '50%'   => __('2 Columns', 'premium-social-media'),
                    '33.33%'=> __('3 Columns', 'premium-social-media'),
                    '25%'   => __('4 Columns', 'premium-social-media'),
                    '20%'   => __('5 Columns', 'premium-social-media'),
                    '16.667%'=> __('6 Columns', 'premium-social-media'),
                ],
                'desktop_default'   => '33.33%',
				'tablet_default'    => '50%',
				'mobile_default'    => '100%',
                'condition'         => [
                    'layout_style' =>  'masonry',
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-social-feed-element-wrap' => 'width: {{VALUE}}'
                ],
            ]
        );

        $this->add_control(
            'direction',
            [
                'label'         => esc_html__( 'Direction', 'premium-social-media' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'ltr'    => [
                        'title' => __( 'Left to Right', 'premium-social-media' ),
                        'icon'  => 'fa fa-chevron-circle-right',
                    ],
                    'rtl'   => [
                        'title' => __( 'Right to Left', 'premium-social-media' ),
                        'icon'  => 'fa fa-chevron-circle-left',
                    ],
                ],
                'default'       => 'ltr',
            ]
        );

        $this->add_responsive_control(
            'align',
            [
                'label'         => esc_html__( 'Content Alignment', 'premium-social-media' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'          => [
                        'title' => __( 'Left', 'premium-social-media' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center'        => [
                        'title' => __( 'Center', 'premium-social-media' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right'          => [
                        'title' => __( 'Right', 'premium-social-media' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                    'justify'       => [
                        'title' => esc_html__( 'Justify', 'premium-social-media' ),
                        'icon'  => 'fa fa-align-justify',
                    ],
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-text, {{WRAPPER}} .premium-read-button' => 'text-align: {{VALUE}}',
                ],
                'default'       => 'left',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'layout_settings',
            [
                'label'         => esc_html__('Advanced Settings', 'premium-social-media' )
            ]
        );

        $this->add_control(
            'post_number',
            [
                'label'         => esc_html__( 'Posts/Account', 'premium-social-media' ),
                'type'          => Controls_Manager::NUMBER,
                'label_block'   => false,
                'description'       => esc_html__( 'How many posts will be shown for each account', 'premium-social-media' ),
                'default'       => 2
            ]
        );

        $this->add_control(
            'content_length',
            [
                'label'         => esc_html__('Post Length','premium-social-media'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 200,
            ]
        );

        $this->add_control(
            'posts_media',
            [
                'label'         => esc_html__('Show Post Media','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'default'       => 'yes',
            ]
        );
        
        $this->add_control(
            'show_avatar', 
            [
                'label'         => esc_html__('Show Avatar','premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'block' => esc_html__('Show', 'premium-social-media'),
                    'none'  => esc_html__('Hide', 'premium-social-media'),
                ],
                'default'       => 'block',
                'selectors'     => [
                    '{{WRAPPER}} .premium-author-img'   => 'display: {{VALUE}}'
                ]
            ]
        );
        
        $this->add_control(
            'show_date', 
            [
                'label'         => esc_html__('Show Date','premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'block' => esc_html__('Show', 'premium-social-media'),
                    'none'  => esc_html__('Hide', 'premium-social-media'),
                ],
                'default'       => 'block',
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-date'   => 'display: {{VALUE}}'
                ]
            ]
        );
        
        $this->add_control(
            'show_read', 
            [
                'label'         => esc_html__('Show Read More Button','premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'block' => esc_html__('Show', 'premium-social-media'),
                    'none'  => esc_html__('Hide', 'premium-social-media'),
                ],
                'default'       => 'block',
                'selectors'     => [
                    '{{WRAPPER}} .premium-read-button'   => 'display: {{VALUE}}'
                ]
            ]
        );
        
        $this->add_control(
            'show_icon', 
            [
                'label'         => esc_html__('Show Twitter Icon','premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'inline-block' => esc_html__('Show', 'premium-social-media'),
                    'none'  => esc_html__('Hide', 'premium-social-media'),
                ],
                'default'       => 'inline-block',
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-icon'   => 'display: {{VALUE}}'
                ]
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'post_box_style',
            [
                'label'         => esc_html__('Post Box','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs( 'post_box' );

        $this->start_controls_tab(
            'post_box_normal',
            [
                'label'         => esc_html__('Normal', 'premium-social-media'),
            ]
        );

        $this->add_control(
            'post_box_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'post_box_border',
                'selector'      => '{{WRAPPER}} .premium-social-feed-element',
            ]
        );

        $this->add_control(
            'post_box_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'post_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-social-feed-element',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'post_box_hover',
            [
                'label'         => esc_html__('Hover', 'premium-social-media'),
            ]
        );

        $this->add_control(
            'post_box_background_hover', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element:hover' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'post_box_border_hover',
                'selector'      => '{{WRAPPER}} .premium-social-feed-element:hover',
            ]
        );

        $this->add_control(
            'post_box_border_radius_hover',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element:hover' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'post_box_shadow_hover',
                'selector'      => '{{WRAPPER}} .premium-social-feed-element:hover',
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'post_box_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'separator'     => 'before',
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'post_box_padding',
                [
                    'label'     => esc_html__('Padding', 'premium-social-media'),
                    'type'      => Controls_Manager::DIMENSIONS,
                    'size_units'=> ['px', 'em', '%'],
                    'selectors' => [
                        '{{WRAPPER}} .premium-social-feed-element' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'content_style',
            [
                'label'         => esc_html__('Content','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'content_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-text' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'facebook_feed_content_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-social-feed-text',
            ]
        );

        $this->add_responsive_control(
            'facebook_feed_content_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-text' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_control(
            'facebook_feed_popover_toggle',
            [
                'label'         => esc_html__('Read More', 'premium-social-media'),
                'type'          => Controls_Manager::POPOVER_TOGGLE,
                'condition'     => [
                    'read'  => 'yes'
                ],
            ]
        );

        $this->start_popover();

        $this->add_control(
            'read_more_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-read-button' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'read_more_color_hover', 
            [
                'label'         => esc_html__('Hover Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-read-button:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'facebook_feed_read_more_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-read-button',
            ]
        );

        $this->end_popover();

        $this->end_controls_section();

        $this->start_controls_section(
            'avatar_style',
            [
                'label'         => esc_html__('Avatar','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'show_avatar'    => 'yes'
                ]
            ]
        );

        $this->add_responsive_control(
            'avatar_size',
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element .media-object ' => 'width: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'avatar_border',
                'selector'      => '{{WRAPPER}} .premium-author-img img',
            ]
        );

        $this->add_control(
            'avatar_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-author-img img' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'avatar_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-author-img img' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'icon_style',
            [
                'label'         => esc_html__('Facebook Icon','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'show_icon'    => 'yes'
                ]
            ]
        );

        $this->add_control(
            'facebook_icon_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-icon' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'facebook_icon_size',
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'icon_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'title_style',
            [
                'label'         => esc_html__('Author','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'title_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-author-title a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'title_hover_color', 
            [
                'label'         => esc_html__('Hover Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-author-title:hover a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'title_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-social-author-title a',
            ]
        ); 

        $this->add_responsive_control(
            'title_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-author-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'date_style',
            [
                'label'         => esc_html__('Date','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'show_date'  => 'yes'
                ]
            ]
        );

        $this->add_control(
            'date_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-date a' => 'color: {{VALUE}};',
                ],
                'separator'     => 'before',
            ]
        );

        $this->add_control(
            'date_hover_color',
            [
                'label'         => esc_html__('Hover Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-date:hover a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'date_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-social-date a',
            ]
        ); 

        $this->add_responsive_control(
            'date_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-date' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'general_style',
            [
                'label'         => esc_html__('Container','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'container_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-facebook-feed-wrapper' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'container_box_border',
                'selector'      => '{{WRAPPER}} .premium-facebook-feed-wrapper',
            ]
        );

        $this->add_control(
            'container_box_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-facebook-feed-wrapper' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'container_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-facebook-feed-wrapper',
            ]
        );

        $this->add_responsive_control(
            'container_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-facebook-feed-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'container_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-facebook-feed-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

	}
    
    /**
	 * renders the HTML content of the widget
	 * @return void
	 */
	protected function render() {

      	$settings = $this->get_settings();
        
//      $container_id = '#premium-social-feed-container-' . $this->get_id();
        
        $layout_class = $settings['layout_style'] == 'list' ? 'list-layout' : 'grid-layout';
        
        $template = $settings['layout_style'] == 'list' ? 'list-template.php' : 'grid-template.php';

        $direction = $settings['direction'];
        
        $limit = !empty( $settings['post_number'] ) ? esc_html( $settings['post_number'] ) : 2;
        
        $post_length = !empty( $settings['content_length'] ) ? esc_html ( $settings['content_length'] ) : 130;
        
        $show_media = ( $settings['posts_media'] == 'yes' ) ? true : false;
        
        $facebook_settings = [
            'accounts'  => esc_html($settings['account_id']),
            'limit'     => $limit,
            'accessTok' => esc_html($settings['access_token']),
            'length'    => $post_length,
            'showMedia' => $show_media,
            'layout'    => $layout_class,
            'template'  => plugins_url( '/templates/', __FILE__ ) . $template,
        ];
        
        $id = $this->get_id();
        
        $this->add_render_attribute('premium_facebook', 'class', [ 'premium-facebook-feed-wrapper', $direction ] );
        $this->add_render_attribute('premium_facebook', 'data-settings', wp_json_encode($facebook_settings) );
        
        $this->add_render_attribute('premium_facebook_inner', 'id', 'premium-social-feed-container-' . $id );
        
        $this->add_render_attribute('premium_facebook_inner', 'class', [ 'premium-social-feed-container', $layout_class ] );
        
	?>

    <?php if( empty ( $settings['access_token'] ) ) : ?>
        <div class="premium-social-error">
                <?php echo esc_html__('Please fill the required fields: App ID & Access Token','premium-social-media'); ?>
        </div>
    <?php else: ?>
        <div <?php echo $this->get_render_attribute_string('premium_facebook'); ?>>
            <div <?php echo $this->get_render_attribute_string('premium_facebook_inner'); ?>></div>
            <div class="premium-loading-feed">
                <div class="premium-loader"></div>
            </div>
        </div>
    <?php endif;
	
	}
}
Plugin::instance()->widgets_manager->register_widget_type( new Premium_Facebook_Feed() );