<?php

/**
 * Class: Premium_Behance_Feed
 * Name: Behance Feed
 * Slug: premium-social-behance
 */

namespace Elementor;

if( ! defined('ABSPATH') ) exit;

class Premium_Behance_Feed extends Widget_Base {
    
    public function get_name() {
        return 'premium-social-behance';
    }
    
    public function get_title() {
        return esc_html__('Premium Behance Feed', 'premium-social-media');
    }
    
    public function get_icon() {
        return 'fa fa-behance';
    }

    public function get_categories() {
        return ['premium-social-media'];
    }
    
    public function get_script_depends() {
        return [
            'ps-behance-js',
            'premium-social-media-js',
        ];
    }
    
    public function is_reload_preview_required() {
        return true;
    }
    
    // Adding the controls fields for the Behance Feed
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls(){
        
        /*Start Access Credentials Section*/
        $this->start_controls_section(
            'access_credentials_section',
            [
                'label'         => esc_html__('Access Credentials', 'premium-social-media')
            ]
        );

        $this->add_control(
            'api_key',
            [
                'label'         => esc_html__( 'API key', 'premium-social-media' ),
                'type'          => Controls_Manager::TEXT,
                'label_block'   => false,
                'default'       => 'XQhsS66hLTKjUoj8Gky7FOFJxNMh23uu',
                'description'   => '<a href="https://www.behance.net/dev" target="_blank">Get API Key.</a> Create or select an app and grab the API Key',
            ]
        );

        $this->add_control(
            'username',
            [
                'label'         => esc_html__( 'Username', 'premium-social-media' ),
                'type'          => Controls_Manager::TEXT,
                'label_block'   => false,
                'default'       => 'rimounadel',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'display',
            [
                'label'         => esc_html__('Display Options', 'premium-social-media')
            ]    
        );

        $this->add_control(
            'feed_column_number',
            [
                'label'         => esc_html__('Number of Columns', 'premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'col-1'         => esc_html__('1 Column', 'premium-social-media'),
                    'col-2'         => esc_html__('2 Columns', 'premium-social-media'),
                    'col-3'         => esc_html__('3 Columns', 'premium-social-media'),
                    'col-4'         => esc_html__('4 Columns', 'premium-social-media'),
                    'col-5'         => esc_html__('5 Columns', 'premium-social-media'),
                    'col-6'         => esc_html__('6 Columns', 'premium-social-media'),
                    'col-7'         => esc_html__('7 Columns', 'premium-social-media'),
                    'col-8'         => esc_html__('8 Columns', 'premium-social-media'),
                ],
                'default'       => 'col-3',     
            ]
        );

        $this->add_control(
            'hover_effect',
            [
                'label'         => esc_html__('Hover Image Effect', 'premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'none'          => esc_html__('None', 'premium-social-media'),
                    'zoomin'        => esc_html__('Zoom In', 'premium-social-media'),
                    'zoomout'       => esc_html__('Zoom Out', 'premium-social-media'),
                    'scale'         => esc_html__('Scale', 'premium-social-media'),
                    'gray'          => esc_html__('Grayscale', 'premium-social-media'),
                    'blur'          => esc_html__('Blur', 'premium-social-media'),
                    'bright'        => esc_html__('Bright', 'premium-social-media'),
                    'sepia'         => esc_html__('Sepia', 'premium-social-media'),
                    'trans'         => esc_html__('Translate', 'premium-social-media'),
                ],
                'default'       => 'zoomin',
                'label_block'   => true
            ]
        );

        $this->add_responsive_control(
            'img_align',
            [
                'label'         => esc_html__( 'Image Alignment', 'premium-social-media' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'          => [
                        'title'=> esc_html__( 'Left', 'premium-social-media' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center'        => [
                        'title'=> esc_html__( 'Center', 'premium-social-media' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right'         => [
                        'title'=> esc_html__( 'Right', 'premium-social-media' ),
                        'icon' => 'fa fa-align-right',
                    ],  
                ],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-project .wrap-cover-outer' => 'text-align: {{VALUE}};',
                    ],
                'default'       => 'center',
            ]
        );

        $this->add_responsive_control(
            'name_align',
            [
                'label'         => esc_html__( 'Name Alignment', 'premium-social-media' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'          => [
                        'title'=> esc_html__( 'Left', 'premium-social-media' ),
                        'icon' => 'fa fa-align-left',
                        ],
                    'center'        => [
                        'title'=> esc_html__( 'Center', 'premium-social-media' ),
                        'icon' => 'fa fa-align-center',
                        ],
                    'right'         => [
                        'title'=> esc_html__( 'Right', 'premium-social-media' ),
                        'icon' => 'fa fa-align-right',
                        ],
                    ],
                'default'       => 'center',
                'condition'     => [
                    'project_name'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-project .wrap-title-text' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'author_align',
            [
                'label'         => esc_html__( 'Author Alignment', 'premium-social-media' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'          => [
                        'title'=> esc_html__( 'Left', 'premium-social-media' ),
                        'icon' => 'fa fa-align-left',
                        ],
                    'center'        => [
                        'title'=> esc_html__( 'Center', 'premium-social-media' ),
                        'icon' => 'fa fa-align-center',
                        ],
                    'right'         => [
                        'title'=> esc_html__( 'Right', 'premium-social-media' ),
                        'icon' => 'fa fa-align-right',
                        ],
                    ],
                'default'       => 'center',
                'condition'     => [
                    'owner'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-owners-outer' => 'justify-content: {{VALUE}};',
                    ],
                'condition'     => [
                    'owner' => 'yes'
                ]
            ]
        );

        $this->add_responsive_control(
            'info_align',
            [
                'label'         => esc_html__( 'Info Alignment', 'premium-social-media' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'          => [
                        'title'=> esc_html__( 'Left', 'premium-social-media' ),
                        'icon' => 'fa fa-align-left',
                        ],
                    'center'        => [
                        'title'=> esc_html__( 'Center', 'premium-social-media' ),
                        'icon' => 'fa fa-align-center',
                        ],
                    'right'         => [
                        'title'=> esc_html__( 'Right', 'premium-social-media' ),
                        'icon' => 'fa fa-align-right',
                        ],
                    ],
                'conditions'    => [
                    'relation'      =>  'or',
                    'terms'         => [
                        [
                            'name'  =>  'appreciate',
                            'value'  => 'yes'
                        ],
                        [
                            'name'  =>  'views',
                            'value'  => 'yes'
                        ]
                    ],  
                ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .wrap-project' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'advanced',
            [
                'label'         => esc_html__('Advanced Settings', 'premium-social-media')
            ]
        );

        $this->add_control(
            'project_name',
            [
                'label'         => esc_html__('Show Project Name','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'default'       => 'yes',
            ]
        );

        $this->add_control(
            'owner',
            [
                'label'         => esc_html__('Show Author','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'default'       => 'yes',
            ]
        );

        $this->add_control(
            'appreciate',
            [
                'label'         => esc_html__('Show Apprectiations','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'default'       => 'yes',
            ]
        );

        $this->add_control(
            'views',
            [
                'label'         => esc_html__('Show Views','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'default'       => 'yes',
            ]
        );

        $this->add_control('heading',
            [
                'label'         => esc_html__('Lightbox', 'premium-social-media'),
                'type'          => Controls_Manager::HEADING
            ]
        );

        $this->add_control(
            'date',
            [
                'label'         => esc_html__('Show Date','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'default'       => 'yes',
            ]
        );

        $this->add_control(
            'url',
            [
                'label'         => esc_html__('Project URL','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'default'       => 'yes',
            ]
        );

        $this->add_control(
            'caption',
            [
                'label'        => esc_html__('Image Caption','premium-social-media'),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => 'Show',
                'label_off'    => 'Hide',
                'default'      => 'yes',
            ]
        );

        $this->add_control(
            'desc',
            [
                'label'         => esc_html__('Description','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
            ]
        );

        $this->add_control(
            'number',
            [
                'label'         => esc_html__('Number of Projects','premium-social-media'),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'default'       => 9
            ]
        );

        $this->add_control(
            'load',
            [
                'label'         => esc_html__('Load More','premium-social-media'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'button',
            [
                'label'         => esc_html__('Button', 'premium-social-media'),
                'condition'     => [
                    'load'  => 'yes'
                ]
            ]
        );

        $this->add_control(
            'button_size', 
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'lg',
                'options'       => [
                    'sm'            => esc_html__('Small','premium-social-media'),
                    'md'            => esc_html__('Medium','premium-social-media'),
                    'lg'            => esc_html__('Large','premium-social-media'),
                    'block'         => esc_html__('Block','premium-social-media'),
                ],
                'label_block'   => true,
            ]
        );

        $this->add_responsive_control(
            'button_align',
            [
                'label'         => esc_html__( 'Alignment', 'premium-social-media' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'    => [
                        'title' => __( 'Left', 'premium-social-media' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'premium-social-media' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'premium-social-media' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .premium-behance-btn' => 'text-align: {{VALUE}}',
                ],
                'default'       => 'center',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'img',
            [
                'label'         => esc_html__('Image', 'premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                ]
            );

        $this->add_responsive_control(
            'image_size',
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'range'             => [
                    'px'    => [
                        'min' => 50, 
                        'max' => 500,
                        ],
                    'em'    => [
                        'min' => 1, 
                        'max' => 100,
                        ]
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-cover' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'img_border',
                'selector'      => '{{WRAPPER}} .wrap-project .wrap-cover',
            ]
        );

        $this->add_control(
            'img_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-project .wrap-cover' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'img_box_shadow',
                'selector'      => '{{WRAPPER}} .wrap-project .wrap-cover',
            ]
        );

        $this->add_responsive_control(
            'img_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-project .wrap-cover' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'overlay_style',
            [
                'label'         => esc_html__('Overlay','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'caption' => 'yes'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'overlay_background',
                'types'         => [ 'classic' , 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-project .wrap-cover .fields-in-cover',
            ]
        );

        $this->start_controls_tabs('overlay_tabs');

        $this->start_controls_tab(
            'overlay_icon_tab',
            [
                'label'         => esc_html__('Icon', 'premium-social-media')
            ]
        );

        $this->add_control(
            'overlay_icon_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover > svg path' => 'fill: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'overlay_icon_size',
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover > svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_control(
            'overlay_icon_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover > svg' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'overlay_icon_border',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover > svg'
            ]
        );

        $this->add_control(
            'overlay_icon_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover > svg' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(), 
            [
                'label'         => esc_html__('Box Shadow','premium-social-media'),
                'name'          => 'overlay_icon_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover > svg'
            ]
        );

        $this->add_responsive_control(
            'overlay_icon_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover > svg' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'overlay_icon_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover > svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'overlay_text_tab',
            [
                'label'         => esc_html__('Tags', 'premium-social-media')
            ]
        );

        $this->add_control(
            'overlay_num_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover .single' => '    color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'overlay_num_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover .single'
            ]
        ); 

        $this->add_control(
            'overlay_num_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover .single' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'overlay_num_border',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover .single'
            ]
        );

        $this->add_control(
            'overlay_num_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover .single' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(), 
            [
                'label'         => esc_html__('Box Shadow','premium-social-media'),
                'name'          => 'overlay_num_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover .single'
            ]
        );

        $this->add_responsive_control(
            'overlay_num_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover .single' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'overlay_num_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-projects li .wrap-cover .fields-in-cover .single' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section(
            'project',
            [
                'label'         => esc_html__('Project', 'premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'project_background',
                'types'         => [ 'classic' , 'gradient' ],
                'selector'      => '{{WRAPPER}} .wrap-projects .wrap-project',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'project_border',
                'selector'      => '{{WRAPPER}} .wrap-projects .wrap-project',
            ]
        );

        $this->add_control(
            'project_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-projects .wrap-project' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'project_box_shadow',
                'selector'      => '{{WRAPPER}} .wrap-projects .wrap-project',
            ]
        );

        $this->add_responsive_control(
            'project_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-projects .wrap-project' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );

        $this->add_responsive_control(
            'project_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-projects .wrap-project' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'title',
            [
                'label'         => esc_html__('Name','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'project_name'  => 'yes'
                ]
            ]
        );

        $this->add_control(
            'title_color', 
            [
                'label'         => esc_html__('Text Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-projects .wrap-title-text' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'title_color_hover', 
        [
            'label'             => esc_html__('Text Hover Color', 'premium-social-media'),
            'type'              => Controls_Manager::COLOR,
            'scheme'            => [
                'type'  => Scheme_Color::get_type(),
                'value' => Scheme_Color::COLOR_1,
                ],
            'selectors'         => [
                '{{WRAPPER}} .wrap-title-text:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'title_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-project .wrap-title-text',
            ]
        ); 

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(), 
            [
                'name'          => 'title_text_shadow',
                'selector'      => '{{WRAPPER}} .wrap-title-text',
            ]
        );

        $this->add_control(
            'title_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}}  .wrap-title-text' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'title_border',
                'selector'      => '{{WRAPPER}} .wrap-title-text',
            ]
        );

        $this->add_control(
            'title_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-title-text' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'title_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-title-text' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'title_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .wrap-title-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'author',
            [
                'label'         => esc_html__('Author','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'owner' => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('author_tabs');

        $this->start_controls_tab(
            'author_label_tab',
            [
                'label'        => esc_html__('Label', 'premium-social-media')
            ]
        );

        $this->add_control(
            'author_label_color', 
            [
                'label'         => esc_html__('Text Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                        'type'      => Scheme_Color::get_type(),
                        'value'     => Scheme_Color::COLOR_2,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-label' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'author_label_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-label',
            ]
        ); 

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(), 
            [
                'name'          => 'author_label_text_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-label',
            ]
        );

        $this->add_control(
            'author_label_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-label' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'author_label_border',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-label',
            ]
        );

        $this->add_control(
            'author_label_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-label' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'author_label_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-label' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'author_label_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-label' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'author_name_tab',
            [
                'label'         => esc_html__('Name', 'premium-social-media')
            ]
        );

        $this->add_control(
            'author_color', 
            [
                'label'         => esc_html__('Text Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .owner-full-name a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'author_color_hover', 
            [
                'label'         => esc_html__('Text Hover Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .owner-full-name:hover a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(), 
            [
                'name'          => 'author_text_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-container .owner-full-name',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'author_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-behance-container .owner-full-name a',
            ]
        ); 

        $this->add_control(
            'author_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .owner-full-name' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'author_border',
                'selector'      => '{{WRAPPER}} .premium-behance-container .owner-full-name',
            ]
        );

        $this->add_control(
            'author_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .owner-full-name' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'author_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .owner-full-name' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'author_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .owner-full-name' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();


        $this->start_controls_section(
            'app',
            [
                'label'         => esc_html__('Apprectiations','premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'appreciate'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('app_tabs');

        $this->start_controls_tab(
            'app_icon_tab',
            [
                'label'         => esc_html__('Icon', 'premium-social-media')
            ]
        );        

        $this->add_control(
            'app_icon_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-label svg g path' => 'fill: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'app_icon_size',
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-label svg' => 'width: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_control(
            'app_icon_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-label' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'app_icon_border',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-label',
            ]
        );

        $this->add_control(
            'app_icon_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-label' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(), 
            [
                'name'          => 'app_icon_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-label'
            ]
        );

        $this->add_responsive_control(
            'app_icon_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-label' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'app_icon_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-label' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'app_num_tab',
            [
                'label'         => esc_html__('Number', 'premium-social-media')
            ]
        );

        $this->add_control(
            'app_num_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-app-value' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'app_num_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-app-value'
            ]
        ); 

        $this->add_control(
            'app_num_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-value' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'app_num_border',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-value',
            ]
        );

        $this->add_control(
            'app_num_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-value' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(), 
            [
                'name'          => 'app_num_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-value'
            ]
        );

        $this->add_responsive_control(
            'app_num_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-value' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'app_num_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-appreciations-outer .wrap-value' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section(
            'views_style',
            [
                'label'             => esc_html__('Views','premium-social-media'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition'         => [
                    'views' => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('views_tabs');

        $this->start_controls_tab(
            'views_icon_tab',
            [
                'label'             => esc_html__('Icon', 'premium-social-media')
            ]
        );        

        $this->add_control(
            'views_icon_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-label svg g path' => 'fill: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'views_icon_size',
            [
                'label'         => esc_html__('Size', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-label svg' => 'width: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_control(
            'views_icon_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-label' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'views_icon_border',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-label',
            ]
        );

        $this->add_control(
            'views_icon_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-label' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
            );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(), 
            [
                'name'          => 'views_icon_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-label'
            ]
        );

        $this->add_responsive_control(
            'views_icon_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-label' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'views_icon_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-label' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'views_num_tab',
            [
                'label'         => esc_html__('Number', 'premium-social-media')
            ]
        );

        $this->add_control(
            'views_num_color', 
            [
                'label'         => esc_html__('Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-view-value' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'views_num_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-view-value'
            ]
        ); 

        $this->add_control(
            'views_num_background', 
            [
                'label'         => esc_html__('Background', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-value' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'views_num_border',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-value',
            ]
        );

        $this->add_control(
            'views_num_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-value' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(), 
            [
                'name'          => 'views_num_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-value'
            ]
        );

        $this->add_responsive_control(
            'views_num_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-value' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'views_num_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-container .wrap-views-outer .wrap-value' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section(
            'button_style_settings',
            [
                'label'         => esc_html__('Button', 'premium-social-media'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'load'  => 'yes',
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
                [
                    'name'          => 'button_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button span',
            ]
        );

        $this->start_controls_tabs('button_style_tabs');

        $this->start_controls_tab(
            'button_style_normal',
            [
                'label'         => esc_html__('Normal', 'premium-social-media'),
            ]
        );

        $this->add_control(
            'button_color',
            [
                'label'         => esc_html__('Text Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-btn .eb-pagination-button span'  => 'color: {{VALUE}};'
                    ]
                ]
            );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'button_text_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'              => 'button_background',
                'types'             => [ 'classic' , 'gradient' ],
                'selector'          => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'button_border',
                'selector'      => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button',
            ]
        );

        $this->add_control(
            'button_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-btn .eb-pagination-button' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'button_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button',
            ]
        );

        $this->add_responsive_control(
            'button_margin',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-btn .eb-pagination-button' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'button_padding',
            [
                'label'         => esc_html__('Padding', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-btn .eb-pagination-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'button_style_hover',
            [
                'label'         => esc_html__('Hover', 'premium-social-media'),
            ]
        );

        $this->add_control(
            'button_hover_color',
            [
                'label'         => esc_html__('Text Hover Color', 'premium-social-media'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-btn .eb-pagination-button:hover span'  => 'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'button_text_shadow_hover',
                'selector'      => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button:hover',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'button_background_hover',
                'types'         => [ 'classic' , 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button:hover',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'button_border_hover',
                'selector'      => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button:hover',
            ]
        );

        $this->add_control(
            'button_border_radius_hover',
            [
                'label'         => esc_html__('Border Radius', 'premium-social-media'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%' ],                    
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-btn .eb-pagination-button:hover' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'button_shadow_hover',
                'selector'      => '{{WRAPPER}} .premium-behance-btn .eb-pagination-button:hover',
            ]
        );

        $this->add_responsive_control(
            'button_margin_hover',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-btn .eb-pagination-button:hover' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control(
            'button_padding_hover',
            [
                'label'         => esc_html__('Margin', 'premium-social-media'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-behance-btn .eb-pagination-button:hover' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );


        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();
        
    }
    
    
    /**
	 * renders the HTML content of the widget
	 * @return void
	 */
    protected function render(){
        
        $settings   = $this->get_settings();
        
        $api_key    = esc_html( $settings['api_key'] );
        
        $username   = esc_html ( $settings['username'] );

        $project    = ( 'yes' == $settings['project_name'] ) ? true : false;
        
        $owner      = ( 'yes' == $settings['owner'] ) ? true : false;
        
        $appreciations = ( 'yes' == $settings['appreciate'] ) ? true : false;
        
        $views      = ( 'yes' == $settings['views'] ) ? true : false;
        
        $date       = ( 'yes' == $settings['date'] ) ? true : false;
        
        $url        = ( 'yes' == $settings['url'] ) ? true : false;
        
        $caption    = ( 'yes' == $settings['caption'] ) ? true : false;
        
        $desc       = ( 'yes' == $settings['desc'] ) ? true : false;
        
        $load_more  = ( 'yes' == $settings['load'] ) ? '' : 'button-none';
        
        $hover_effect = $settings['hover_effect'];
        
        $photos_num = ! empty( $settings['number'] ) ? esc_html( $settings['number'] ) : 1;
        
        $col_num    = $settings['feed_column_number'];
        
        $button_size = $settings['button_size'];
        
        $id = $this->get_id();
        
        $this->add_render_attribute('premium_behance', 'id', 'premium-behance-container-' . $id );
        
        $this->add_render_attribute('premium_behance', 'class', [ 'premium-behance-container', 'premium-behance-' . $col_num, $button_size, $load_more, $hover_effect ] );
        
        $behance_settings = [
            'api_key'       => $api_key,
            'username'      => $username,
            'project'       => $project,
            'owner'         => $owner,
            'apprectiations'=> $appreciations,
            'views'         => $views,
            'fields'        => $caption,
            'date'          => $date,
            'url'           => $url,
            'desc'          => $desc,
            'id'            => $this->get_id(),
            'number'        => $photos_num
        ];
        
        $this->add_render_attribute('premium_behance', 'data-settings', wp_json_encode($behance_settings) );
        
        ?>

    <?php if( empty ( $settings['api_key'] ) || empty ( $settings['username'] ) ) : ?>
        <div class="premium-social-error">
                <?php echo esc_html__('Please fill the required fields: App ID & Access Token','premium-social-media'); ?>
        </div>
    <?php else: ?>
        <div <?php echo $this->get_render_attribute_string('premium_behance'); ?>></div>
        <div class="premium-loading-feed">
            <div class="premium-loader"></div>
        </div>
        
    <?php endif; ?>
        
    <?php }
}

PLUGIN::instance()->widgets_manager->register_widget_type( new Premium_Behance_Feed() );