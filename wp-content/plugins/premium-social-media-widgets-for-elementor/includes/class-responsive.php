<?php

use Elementor\Core\Responsive\Responsive;

if ( ! defined( 'ABSPATH' ) ) exit();

if( ! class_exists('Premium_Social_Responsive') ) {
    
    class Premium_Social_Responsive {
    
        private static $instance = null;
        
        public function __construct() {
            
            // Enqueue responsive style
            add_action( 'elementor/frontend/after_enqueue_styles', array( $this, 'premium_social_media_responsive_style' ) );
            
        }
        
        /**
         * Adds the required style based on Elementor's breakpoints
         */
        public function premium_social_media_responsive_style() {
            
            $breakpoints = Responsive::get_breakpoints();
            
            ?>

            <style>
                @media ( max-width: <?php echo $breakpoints['lg']; ?>px ) {
                    .premium-behance-container .wrap-project {
                        flex-basis: 50% !important;
                        -ms-flex-preferred-size: 50% !important;
                    }
                }
                @media ( max-width: <?php echo $breakpoints['md']; ?>px ) {
                    .premium-behance-container .wrap-project {
                        flex-basis: 100% !important;
                        -ms-flex-preferred-size: 100% !important;
                    }
                }
            </style>

        <?php }
        
        public static function get_instancce() {
            
            if ( self::$instance == null ) {
                
                self::$instance = new self;
                
            }
            
            return self::$instance;
            
        }
    
    }
}

/**
 * Returns instance of Premium_Social_Responsive
 *
 * @return object
 */
if ( ! function_exists('premium_social_responsive') ) {
    
    function premium_social_responsive() {
        return Premium_Social_Responsive::get_instancce();
    }
   
}
premium_social_responsive();