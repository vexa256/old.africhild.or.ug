( function( $, elementor ) {
    
    // Twitter Feed Handler
    var PremiumTwitterFeedHandler = function ( $scope,$ ){
        
        var premiumTwitterFeedElement   = $scope.find( '.premium-twitter-feed-wrapper' ),
            loadingFeed                 = premiumTwitterFeedElement.find('.premium-loading-feed'),
            premiumTwitterSettings      = premiumTwitterFeedElement.data( 'settings' );

        function get_tweets_data() {
            premiumTwitterFeedElement.find( '.premium-social-feed-container' ).socialfeed({
                twitter: {
                    accounts:       premiumTwitterSettings['accounts'],
                    limit:          premiumTwitterSettings['limit'],
                    consumer_key:   premiumTwitterSettings['consKey'],
                    consumer_secret: premiumTwitterSettings['consSec'],
                    token:          "460616970-Deuil3Qx0CnNS2VX9WefxA99gD8OFx1vJ0kn0izb",
                    secret:         "GBdekapULnR5iCiLozWQMc9xGYhwZlVO2zKXcpBb7AFFT",
                    tweet_mode:     'extended'
                },
                length:             premiumTwitterSettings['length'],
                show_media:         premiumTwitterSettings['showMedia'],
                template:           premiumTwitterSettings['template']
            });
        }
        
        function twitter_masonry_grid() {
            var masonryContainer = premiumTwitterFeedElement.find( '.premium-social-feed-container' );
            
            masonryContainer.imagesLoaded( function() {
                
                $( masonryContainer ).masonry({
                    itemSelector:   '.premium-social-feed-element-wrap',
                    percentPosition: true
                });
                
            });
                
        }
        
        $.ajax({
		   	url: get_tweets_data(),
            beforeSend: function() {
		   		loadingFeed.addClass( 'premium-show-loading' );
		   	},
		   	success: function() {
                if( premiumTwitterSettings['layout'] === 'grid-layout' ) {
                    setTimeout(function() {
                        twitter_masonry_grid();
                    }, 1500 );
                }
                loadingFeed.removeClass( 'premium-show-loading' );
			},
			error: function() {
				console.log('error getting data from Twitter');
			}
        });
        
    };

    // Instagram Feed Handler
    var instaCounter = 0;
    var PremiumInstaFeedHandler = function($scope,$){
        instaCounter++;
        var premiumInstaElem = $scope.find('.premium-instafeed-container'),
            loadingFeed     = premiumInstaElem.find('.premium-loading-feed'),
            premiumInstaSettings = premiumInstaElem.data('settings');
    
        var template = '<div class="premium-insta-feed premium-insta-box '+ premiumInstaSettings['hoverEff'] + '"><div class="premium-insta-feed-inner"><div class="premium-insta-feed-wrap"><div class="premium-insta-img-wrap"><img src="{{image}}"/></div><div class="premium-insta-info-wrap"><div class="premium-insta-likes-comments">'+ premiumInstaSettings['likes'] + premiumInstaSettings['comments'] + '</div></div>'+ premiumInstaSettings['link'] +'</div></div></div>';
        
        function premium_insta_feed(){
            
            var feed = new Instafeed({
                    target: premiumInstaSettings['id'],
                    clientId: premiumInstaSettings['clientId'],
                    accessToken: premiumInstaSettings['accesstok'],
                    get: premiumInstaSettings['get'],
                    userId: premiumInstaSettings['user'],
                    locationId: premiumInstaSettings['location'],
                    tagName: premiumInstaSettings['tag'],
                    sortBy: premiumInstaSettings['sort'],
                    limit: premiumInstaSettings['limit'],
                    resolution: premiumInstaSettings['res'],
                    template: template
                });
                
            try{
                feed.run();
            } catch(err){
                console.log(err);
            }
            
        };
        
        $(window).load(function(){
            premiumInstaElem.imagesLoaded(function () {
                $(premiumInstaElem).find(".premium-insta-feed-wrap a[data-rel^='prettyPhoto']").prettyPhoto({
                    theme: 'pp_default',
                    hook: 'data-rel',
                    opacity: 0.7,
                    show_title: false,
                    deeplinking: false,
                    overlay_gallery: false,
                    custom_markup: '',
                    default_width: 900,
                    default_height: 506,
                    social_tools: ''
                });
            });   
        });
        
        function instagram_masonry_grid() {
            $( premiumInstaElem ).masonry({
                itemSelector:   '.premium-insta-feed',
                percentPosition: true
            });
        }
        
        if( premiumInstaElem.closest('.premium-magic-section-wrap').length === 1 ){
            if( instaCounter === 1){
                $.ajax({
                    url: premium_insta_feed(),
                    beforeSend: function(){
                        loadingFeed.addClass('premium-show-loading') ;
                    },
                    success: function() {
                        if( premiumInstaSettings['masonry'] ) {
                            setTimeout(function(){
                                premiumInstaElem.imagesLoaded(function(){
                                    instagram_masonry_grid();
                                });
                            }, 2000);
                        }
                        loadingFeed.removeClass( 'premium-show-loading' );
                    }
                });        
            }
        } else {
            $.ajax({
                url: premium_insta_feed(),
                beforeSend: function(){
                    loadingFeed.addClass('premium-show-loading') ;
                },
                success: function() {
                    if( premiumInstaSettings['masonry'] ) {
                        setTimeout(function(){
                            premiumInstaElem.imagesLoaded(function(){
                                instagram_masonry_grid();
                            });
                        }, 2000);
                    }
                    loadingFeed.removeClass( 'premium-show-loading' );
                }
            });    
        }
    
    };
    
    // Facebook Feed Handler
    var PremiumFacebookHandler = function ($scope,$){
        var premiumFacebookFeedElement  = $scope.find('.premium-facebook-feed-wrapper'),
            loadingFeed                 = premiumFacebookFeedElement.find('.premium-loading-feed'),
            premiumFacebookSettings     = premiumFacebookFeedElement.data('settings');
        
        function get_facebook_data (){
            premiumFacebookFeedElement.find('.premium-social-feed-container').socialfeed({
                facebook: {
                    accounts:       [premiumFacebookSettings['accounts']],
                    limit:          premiumFacebookSettings['limit'],
                    access_token:   premiumFacebookSettings['accessTok']
                },
                length:             premiumFacebookSettings['length'],
                show_media:         premiumFacebookSettings['showMedia'],
                template:           premiumFacebookSettings['template']
            });
        }
        
        function facebook_masonry_grid() {
            var masonryContainer = premiumFacebookFeedElement.find( '.premium-social-feed-container' );
            $( masonryContainer ).masonry({
                itemSelector:   '.premium-social-feed-element-wrap',
                percentPosition: true
            });
        }
        
        
        $.ajax({
            url: get_facebook_data(),
            beforeSend: function(){
               loadingFeed.addClass('premium-show-loading');
            },
            success: function() {
                if( premiumFacebookSettings['layout'] === 'grid-layout' ) {
                    setTimeout(function() {
                        facebook_masonry_grid();
                    }, 2000);
                }
                loadingFeed.removeClass( 'premium-show-loading' );
			},
			error: function() {
				console.log('error getting data from Facebook');
			}
        });
  
    };
    
    // Behance Feed Handler
    var PremiumBehanceFeedHandler = function($scope,$){
        var premiumBehanceElem = $scope.find('.premium-behance-container'),
            loadingFeed        = $scope.find('.premium-loading-feed'),
            premiumBehanceSettings = premiumBehanceElem.data('settings');

        function get_behance_data() {
            premiumBehanceElem.embedBehance({
                apiKey:  premiumBehanceSettings['api_key'],
                userName: premiumBehanceSettings['username'],
                project: premiumBehanceSettings['project'],
                owners: premiumBehanceSettings['owner'],
                appreciations: premiumBehanceSettings['apprectiations'],
                views: premiumBehanceSettings['views'],
                publishedDate: premiumBehanceSettings['date'],
                fields: premiumBehanceSettings['fields'],
                projectUrl: premiumBehanceSettings['url'],
                infiniteScrolling: false,
                description: premiumBehanceSettings['desc'],
                animationEasing: 'easeInOutExpo',
                ownerLink: true,
                tags: true,
                containerId: premiumBehanceSettings['id'],
                itemsPerPage: premiumBehanceSettings['number']
            });
        }

        $.ajax({
            url: get_behance_data(),
            beforeSend: function() {
                loadingFeed.addClass( 'premium-show-loading' );
            },
            success: function() {
                loadingFeed.removeClass( 'premium-show-loading' );
            },
            error: function() {
                console.log('error getting data from Behance');
            }
        });
        
    };
    
    $(window).on('elementor/frontend/init', function () {
        
        elementorFrontend.hooks.addAction( 'frontend/element_ready/premium-social-twitter.default', PremiumTwitterFeedHandler );
        
        elementorFrontend.hooks.addAction( 'frontend/element_ready/premium-social-instagram.default', PremiumInstaFeedHandler );
        
        elementorFrontend.hooks.addAction( 'frontend/element_ready/premium-social-facebook.default', PremiumFacebookHandler );
        
        elementorFrontend.hooks.addAction( 'frontend/element_ready/premium-social-behance.default', PremiumBehanceFeedHandler );
        
    });    
}( jQuery, window.elementorFrontend ) );